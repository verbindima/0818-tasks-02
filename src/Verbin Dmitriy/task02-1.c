#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define ARR_SIZE 20

int GetMaxDigitString(int * arr, int length)
{
	int i = 0, digit, num = 0, max_num = 0;
	while (i < length) {
		digit = arr[i];
		while (i < length && arr[i] == digit) {
			num++;
			i++;
		}
		if (num > max_num)
			max_num = num;
		num = 0;
	}
	return max_num;
}

int main()
{
	srand(time(0));
	int arr[ARR_SIZE];
	puts("String: ");
	for (int i = 0; i < ARR_SIZE; i++) {
		arr[i] = rand() % 3;
		printf("%i", arr[i]);
	}
	printf("\nMax Digit String: %d\n", GetMaxDigitString(arr, ARR_SIZE));
	return 0;
}