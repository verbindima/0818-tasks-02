#include <stdio.h>

int PalindromOrNo(char * arr, int length)
{
	int center = length / 2;
	for (int i=0; i<center; i++) {
		if (arr[i] != arr[length - 1 - i])
			return 0;
	}
	return 1;
	
}
int main()
{
	char str1[] = "1232141", str2[] = "123321", str3[] = "1ac2321";
	printf("%s :: Palindrom: %d\n", str1, PalindromOrNo(str1, sizeof(str1) / sizeof(char) - 1));
	printf("%s :: Palindrom: %d\n", str2, PalindromOrNo(str2, sizeof(str2) / sizeof(char) - 1));
	printf("%s :: Palindrom: %d\n", str3, PalindromOrNo(str3, sizeof(str3) / sizeof(char) - 1));
	return 0;
}
