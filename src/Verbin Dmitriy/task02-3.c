#include <stdio.h>
#include <stdlib.h>
#include <locale.h>
#include <conio.h>
#include <time.h>

#define n 10
int maxstrmas(int *arr)
{	
	
	int i, j, max=0, count=0;
	for (i=0; i<n-1; i++)
	{
		for (j=i+1; j<n; j++)
			if (arr[i]==arr[j])
				count++;
		if (count>max)
			max=count;
		count=0;
	}
	return max;
}

int maxcount(int (*p)[n])
{
	int i, max=0, str;
	for (i=0; i<n; i++)
	{
		if (maxstrmas(p[i])>max)
		{
			max=maxstrmas(p[i]);
			str=i;
		}
	}
	return str;
}

int main()
{
	
	int matr[n][n];
	int i,j;
	srand(time(0));
	setlocale(LC_ALL, "rus");
	for (i=0; i<n; i++)
		for (j=0; j<n; j++)
			matr[i][j]=rand()%13;
	printf("���� �������:\n");
	for (i=0; i<n; i++)
	{
		for (j=0; j<n; j++)
			printf("%5d",matr[i][j]);
		printf("\n");
	}
	printf("����� ������: %d", maxcount(matr)+1);
	getch();
	return 0;
}