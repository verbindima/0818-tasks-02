#include <stdio.h>
#include <stdlib.h>
#include <locale.h>
#include <time.h>
#include <conio.h>
#define n 10

int maxsumel(int (*p)[n])
 {
	int i,j,st=0,max=0,sum=0;
	for (j=0; j<n; j++)
	{
		for (i=0; i<n; i++)
			sum+=p[i][j];
		if (max<sum)
		{
			max=sum;
			st=j;
		}
		sum=0;
	}
	return st;
}

int main()
{
	int matr[n][n];
	int i,j;
	srand(time(0));
	setlocale(LC_ALL, "rus");
	for (i=0; i<n; i++)
		for (j=0; j<n; j++)
			matr[i][j]=rand()%13;
	printf("���� �������:\n");
	for (i=0; i<n; i++)
	{
		for (j=0; j<n; j++)
			printf("%5d",matr[i][j]);
		printf("\n");
	}
	printf("����� ������� � ������������ ������ ���������: %d", maxsumel(matr)+1);
	getch();
	return 0;
}